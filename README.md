# GoPkg

| Project     | Import                | Version             | License             | Description                                                               |
|-------------|-----------------------|---------------------|---------------------|---------------------------------------------------------------------------|
| [GD]        | `gopkg.org/gd`        | ![GDVersion]        | ![GDLicense]        | This package implement libgd.                                             |
| [Generic]   | `gopkg.org/generic`   | ![GenericVersion]   | ![GenericLicense]   | This package provides helper functions for generic types.                 |
| [Header]    | `gopkg.org/header`    | ![HeaderVersion]    | ![HeaderLicense]    | This package provides the HTTP header constants available on MDN Web Doc. |
| [MIME]      | `gopkg.org/mime`      | ![MIMEVersion]      | ![MIMELicense]      | This package provides the MIME types constants available on IANA.         |
| [Proc]      | `gopkg.org/proc`      | ![ProcVersion]      | ![ProcLicense]      | Provides a library to read information from `/proc`.                      |
| [Random]    | `gopkg.org/random`    | ![RandomVersion]    | ![RandomLicense]    | Makes random string with multi randomizers.                               |
| [Size]      | `gopkg.org/size`      | ![SizeVersion]      | ![SizeLicense]      | Parse and convert SI and CEI size.                                        |
| [Smem]      | `gopkg.org/smem`      | ![SmemVersion]      | ![SmemLicense]      | Implement smem python package.                                            |
| [SwaggerUI] | `gopkg.org/swaggerui` | ![SwaggerUIVersion] | ![SwaggerUILicense] | Provide http.Handler with SwaggerUI embed, and configurable.              |
| [Types]     | `gopkg.org/types`     | ![TypesVersion]     | ![TypesLicense]     | Provides types with Marshaler and Unmarshaler.                            |

[GD]: https://gitlab.com/gopkg-org/gd

[GDVersion]: https://img.shields.io/gitlab/v/tag/gopkg-org%2Fgd?label=%20&style=for-the-badge

[GDLicense]: https://img.shields.io/gitlab/license/gopkg-org%2Fgd?label=%20&style=for-the-badge

[Generic]: https://gitlab.com/gopkg-org/generic

[GenericVersion]: https://img.shields.io/gitlab/v/tag/gopkg-org%2Fgeneric?label=%20&style=for-the-badge

[GenericLicense]: https://img.shields.io/gitlab/license/gopkg-org%2Fgeneric?label=%20&style=for-the-badge

[Header]: https://gitlab.com/gopkg-org/header

[HeaderVersion]: https://img.shields.io/gitlab/v/tag/gopkg-org%2Fheader?label=%20&style=for-the-badge

[HeaderLicense]: https://img.shields.io/gitlab/license/gopkg-org%2Fheader?label=%20&style=for-the-badge

[MIME]: https://gitlab.com/gopkg-org/mime

[MIMEVersion]: https://img.shields.io/gitlab/v/tag/gopkg-org%2Fmime?label=%20&style=for-the-badge

[MIMELicense]: https://img.shields.io/gitlab/license/gopkg-org%2Fmime?label=%20&style=for-the-badge

[Proc]: https://gitlab.com/gopkg-org/proc

[ProcVersion]: https://img.shields.io/gitlab/v/tag/gopkg-org%2Fproc?label=%20&style=for-the-badge

[ProcLicense]: https://img.shields.io/gitlab/license/gopkg-org%2Fproc?label=%20&style=for-the-badge

[Random]: https://gitlab.com/gopkg-org/random

[RandomVersion]: https://img.shields.io/gitlab/v/tag/gopkg-org%2Frandom?label=%20&style=for-the-badge

[RandomLicense]: https://img.shields.io/gitlab/license/gopkg-org%2Frandom?label=%20&style=for-the-badge

[Size]: https://gitlab.com/gopkg-org/size

[SizeVersion]: https://img.shields.io/gitlab/v/tag/gopkg-org%2Fsize?label=%20&style=for-the-badge

[SizeLicense]: https://img.shields.io/gitlab/license/gopkg-org%2Fsize?label=%20&style=for-the-badge

[Smem]: https://gitlab.com/gopkg-org/smem

[SmemVersion]: https://img.shields.io/gitlab/v/tag/gopkg-org%2Fsmem?label=%20&style=for-the-badge

[SmemLicense]: https://img.shields.io/gitlab/license/gopkg-org%2Fsmem?label=%20&style=for-the-badge

[SwaggerUI]: https://gitlab.com/gopkg-org/swaggerui

[SwaggerUIVersion]: https://img.shields.io/gitlab/v/tag/gopkg-org%2Fswaggerui?label=%20&style=for-the-badge

[SwaggerUILicense]: https://img.shields.io/gitlab/license/gopkg-org%2Fswaggerui?label=%20&style=for-the-badge

[Types]: https://gitlab.com/gopkg-org/types

[TypesVersion]: https://img.shields.io/gitlab/v/tag/gopkg-org%2Ftypes?label=%20&style=for-the-badge

[TypesLicense]: https://img.shields.io/gitlab/license/gopkg-org%2Ftypes?label=%20&style=for-the-badge
